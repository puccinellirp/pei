import React from 'react';
import Routes from './src/routes';

// import { Container } from './styles';

const App = () => (
  <>
    <Routes />
  </>
);

export default App; 