import React from 'react';

import { View, Text } from 'react-native';
import CardConvideAmigos from '../../components/NotificationsTemplateCards/CardConvideAmigos';
import CardNovasAulas from '../../components/NotificationsTemplateCards/CardNovasAulas';
import CardNovoSimuladoNotif from '../../components/NotificationsTemplateCards/CardNovoSimuladoNotif';

function NotificationsItem(props) {
    let type = props.type;
    let text = props.text;
    let teacher = props.teacher;
    switch (type) {
        case 'Convide-Amigos':
            return (
                <CardConvideAmigos />
            )
        case 'Novas-Aulas':
            return (
                <CardNovasAulas />
            )
        case 'Novo-Simulado':
            return (
                <CardNovoSimuladoNotif text={text} teacher={teacher} />
            )
        default:
            return (
                <View><Text>Por Favor digite um tipo de Card válido!</Text></View>
            )
    }
}

export default NotificationsItem;
