import React from 'react';

import { View, Text } from 'react-native';
import ConcluidoCard from '../../components/ConquistasTemplateCards/ConcluidoCard';
import EmProgressoCard from '../../components/ConquistasTemplateCards/EmProgressoCard';
import ProntoCard from '../../components/ConquistasTemplateCards/ProntoCard';

function ConquistaItem(props) {
    let type = props.type;
    switch (type) {
        case 'Concluido':
            return (
                <ConcluidoCard />
            )
        case 'Em-Progresso':
            return (
                <EmProgressoCard />
            )
        case 'Pronto':
            return (
                <ProntoCard />
            )
        default:
            return (
                <View><Text>Por Favor digite um tipo de Card válido!</Text></View>
            )
    }
}

export default ConquistaItem;
