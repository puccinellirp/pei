import React from 'react';
import { Image, StatusBar } from 'react-native';
import { createAppContainer } from 'react-navigation'
import { createStackNavigator } from 'react-navigation-stack'

import logo from './assets/gabarito_logo.png';

import Menu from './pages/Menu';
import Feed from './pages/Feed';
import Notificacoes from './pages/Notificacoes';
import Trilha from './pages/Trilha';
import Conquistas from './pages/Conquistas';
import DetalheAula from './pages/DetalheAula';
import Simulados from './pages/Simulados';
import Aulas from './pages/Aulas';
import Avatar from './pages/Avatar';
import Perfil from './pages/Perfil';
import ListaAulas from './pages/ListaAulas';
import Bot from './pages/Bot';

const Routes = createAppContainer(
    createStackNavigator({
        Bot: {
            screen: Bot,
            navigationOptions: () => ({
                headerTitle: 'PEI',
                headerStyle: {
                    marginTop: StatusBar.currentHeight,
                    backgroundColor: '#ffff'
                }
            })
        },
        Menu: {
            screen: Menu,
            navigationOptions: () => ({
                headerTitle: <Image source={logo} />,
                headerStyle: {
                    marginTop: StatusBar.currentHeight,
                    backgroundColor: '#f46522'
                }
            })
        },
        Feed: {
            screen: Feed,
            navigationOptions: () => ({
                headerTitle: 'Feed',
                headerStyle: {
                    marginTop: StatusBar.currentHeight,
                    backgroundColor: '#ffffff'
                }
            })
        },
        Notificacoes: {
            screen: Notificacoes,
            navigationOptions: () => ({
                headerTitle: 'Notificações',
                headerStyle: {
                    marginTop: StatusBar.currentHeight,
                    backgroundColor: '#ffffff'
                }
            })
        },
        Trilha: {
            screen: Trilha,
            navigationOptions: () => ({
                headerTitle: 'Trilha',
                headerStyle: {
                    marginTop: StatusBar.currentHeight,
                    backgroundColor: '#ffffff'
                }
            })
        },
        Conquistas: {
            screen: Conquistas,
            navigationOptions: () => ({
                headerTitle: 'Conquistas',
                headerStyle: {
                    marginTop: StatusBar.currentHeight,
                    backgroundColor: '#ffffff'
                }
            })
        },
        Simulados: {
            screen: Simulados,
            navigationOptions: () => ({
                headerTitle: 'Simulados',
                headerStyle: {
                    marginTop: StatusBar.currentHeight,
                    backgroundColor: '#ffffff'
                }
            })
        },
        Aulas: {
            screen: Aulas,
            navigationOptions: () => ({
                headerTitle: 'Aulas',
                headerStyle: {
                    marginTop: StatusBar.currentHeight,
                    backgroundColor: '#ffffff'
                }
            })
        },
        Avatar: {
            screen: Avatar,
            navigationOptions: () => ({
                headerTitle: 'Avatar',
                headerStyle: {
                    marginTop: StatusBar.currentHeight,
                    backgroundColor: '#ffffff'
                }
            })
        },
        Perfil: {
            screen: Perfil,
            navigationOptions: () => ({
                headerTitle: 'Perfil',
                headerStyle: {
                    marginTop: StatusBar.currentHeight,
                    backgroundColor: '#ffffff'
                }
            })
        },
        ListaAulas: {
            screen: ListaAulas,
            navigationOptions: () => ({
                headerTitle: 'Aulas',
                headerStyle: {
                    marginTop: StatusBar.currentHeight,
                    backgroundColor: '#ffffff'
                }
            })
        },
        DetalheAula: {
            screen: DetalheAula,
            navigationOptions: () => ({
                headerTitle: 'Aula Exemplo',
                headerStyle: {
                    marginTop: StatusBar.currentHeight,
                    backgroundColor: '#ffffff'
                }
            })
        }
    }, {
        headerLayoutPreset: 'center',
        initialRouteName: 'Menu',
    }
    ));

export default Routes;