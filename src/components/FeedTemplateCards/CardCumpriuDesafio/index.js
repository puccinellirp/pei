import React from 'react';

import { View, TouchableOpacity, Text, Image } from 'react-native';
import Icon from "react-native-vector-icons/MaterialIcons";
import { Card } from 'react-native-shadow-cards';
import notification from '../../../assets/notifications.png'
import { Container, CardTitle, CardButton } from './styles';

const CardCumpriuDesafio = () => (
    <Container>
        <Card style={{ width: '100%', paddingTop: 10, paddingBottom: 10 }} cornerRadius={0} elevation={3}>
            <CardTitle><Image source={notification} style={{ width: 20, height: 20, textAlign: 'center' }} />
                <Text style={{ color: '#0c1c3f' }}>Gustavo cumpriu um desafio</Text></CardTitle>
            <View style={{ paddingTop: 10, paddingBottom: 5, paddingRight: 40, paddingLeft: 40, }}>
                <Text>Convide 2 amigos para o PEI</Text>
                <Text style={{ color: '#f46522' }}>Faça-o também</Text>
            </View>
            <View style={{ display: 'flex', flexDirection: 'row', justifyContent: 'space-around', alignItems: 'center' }}>
                <Text style={{ marginRight: 10, fontSize: 12 }}>26/09/2019 às 11h10</Text>
                <TouchableOpacity style={{ width: '40%' }}>
                    <CardButton>Convidar amigos</CardButton>
                </TouchableOpacity>
            </View>
        </Card>
    </Container>
);

export default CardCumpriuDesafio;
