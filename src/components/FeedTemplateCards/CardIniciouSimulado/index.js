import React from 'react';

import { View, TouchableOpacity, Text, Image } from 'react-native';
import Icon from "react-native-vector-icons/MaterialIcons";
import { Card } from 'react-native-shadow-cards';
import icon from '../../../assets/iniciousimulado.png'
import { Container, CardTitle, CardButton } from './styles';

const CardIniciouSimulado = () => (
    <Container>
        <Card style={{ width: '100%', paddingTop: 10, paddingBottom: 10 }} cornerRadius={0} elevation={3}>
            <CardTitle><Image source={icon} style={{ width: 20, height: 20, textAlign: 'center' }} /><Text style={{ color: '#f46522' }}>Sabrina iniciou um simulado</Text></CardTitle>
            <View style={{ paddingTop: 10, paddingBottom: 5, paddingRight: 40, paddingLeft: 40, }}>
                <Text>Simulado de Matemática</Text>
                <Text style={{ color: '#f46522' }}>Faça-o também!</Text>
            </View>
            <View style={{ display: 'flex', flexDirection: 'row', justifyContent: 'space-around', alignItems: 'center' }}>
                <Text style={{ marginRight: 10, fontSize: 12 }}>26/09/2019 às 11h10</Text>
                <TouchableOpacity style={{ width: '40%' }}>
                    <CardButton>Fazer Simulado</CardButton>
                </TouchableOpacity>
            </View>
        </Card>
    </Container>
);

export default CardIniciouSimulado;
