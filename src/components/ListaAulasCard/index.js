import React from 'react';

import { View, TouchableOpacity, Text, Image } from 'react-native';
import Icon from "react-native-vector-icons/MaterialIcons";
import { Card } from 'react-native-shadow-cards';
import notification from '../../assets/notifications-white.png'
import { Container, CardTitle, CardButton } from './styles';

const ListaAulasCard = (props) => (
    <Container style={{ borderTopColor: !props.read ? '#f46522' : '#0C1C3F' }}>
        <Card style={{ width: '100%', padding: 20, marginBottom: 10 }} cornerRadius={0} elevation={3}>
            {!props.read ? <View style={{ backgroundColor: '#f46522', width: '20%', display: 'flex', flexDirection: 'row', alignItems: 'center', padding: 3, position: 'absolute' }}><Image source={notification} style={{ width: 15, height: 15 }} /><Text style={{ color: 'white', padding: 3 }}>Novo</Text></View> :
                <View style={{ backgroundColor: '#707070', width: '50%', display: 'flex', flexDirection: 'row-reverse', padding: 3, position: 'absolute', right: 0 }}><Text style={{ color: 'white', fontSize: 11 }}>Ultimo acesso em 10/09/2019</Text></View>}
            <View style={{ display: 'flex', alignItems: 'center' }}>
                <CardTitle><View style={{
                    textAlign: 'center', backgroundColor: !props.read ? '#f46522' : '#0C1C3F',
                    padding: 5,
                    borderRadius: 30
                }}>
                    <Image source={notification} style={{
                        width: 25, height: 25
                    }} />
                </View></CardTitle>
                <Text style={{ margin: 5, fontSize: 15 }}>{props.name}</Text>
                <TouchableOpacity style={{ width: '40%', display: 'flex', alignItems: 'center' }} onPress={() => props.navigation.push("DetalheAula")}>
                    <CardButton>Estudar</CardButton>
                </TouchableOpacity>
            </View>
        </Card>
    </Container>
);

export default ListaAulasCard;
