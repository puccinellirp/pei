
import styled from 'styled-components/native';

export const Container = styled.View`
  borderTopWidth: 2px;
`;

export const CardTitle = styled.View`
    display: flex;
    flexDirection: row;
    justifyContent: center;
    alignItems: center;
    margin:5px
    `;
export const CardButton = styled.Text`
  color: white;
  marginTop:5px
  font-weight: bold;
  paddingTop: 5px;
  paddingBottom: 5px;
  textAlign: center;
  fontSize:15px;
  backgroundColor: #19ac2d;
  borderRadius: 30px;
  width:80%;
`;