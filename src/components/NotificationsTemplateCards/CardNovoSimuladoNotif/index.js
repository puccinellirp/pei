import React from 'react';

import { View, TouchableOpacity, Text, Image } from 'react-native';
import Icon from "react-native-vector-icons/MaterialIcons";
import { Card } from 'react-native-shadow-cards';
import notification from '../../../assets/notifications-white.png'
import { CardTitle, CardButton } from './styles';

const CardNovoSimuladoNotif = (props) => (
    <Card style={{
        width: '47%', paddingTop: 10, paddingBottom: 10, marginTop: 15,
        borderTopWidth: 2.5,
        borderTopColor: '#f46522'
    }} cornerRadius={0} elevation={3}>
        <CardTitle><View style={{
            textAlign: 'center', backgroundColor: '#f46522',
            padding: 5,
            borderRadius: 20
        }}>
            <Image source={notification} style={{
                width: 15, height: 15
            }} />
        </View><Text style={{ marginLeft: 5, fontSize: 10 }}>26/09/2019 às 11h10</Text></CardTitle>
        <View style={{ padding: 12 }}>
            <Text style={{ fontSize: 13 }}>{props.teacher}</Text>
            <Text style={{ fontSize: 13 }}>{props.text}</Text>
        </View>
        <View style={{ display: 'flex', flexDirection: 'row', justifyContent: 'space-around', alignItems: 'center' }}>
            <TouchableOpacity style={{ width: '65%' }}>
                <CardButton>Fazer Simulado</CardButton>
            </TouchableOpacity>
        </View>
    </Card>
);

export default CardNovoSimuladoNotif;