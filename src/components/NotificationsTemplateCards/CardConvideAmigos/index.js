import React from 'react';

import { View, TouchableOpacity, Text, Image } from 'react-native';
import { Card } from 'react-native-shadow-cards';
import notification from '../../../assets/notifications-white.png'
import { CardTitle, CardButton } from './styles';

const CardConvideAmigos = () => (
    <Card style={{
        width: '47%', paddingTop: 10, paddingBottom: 10, marginTop: 15,
        borderTopWidth: 2.5,
        borderTopColor: '#f46522'
    }} cornerRadius={0} elevation={3}>
        <CardTitle><View style={{
            textAlign: 'center', backgroundColor: '#0c1c3f',
            padding: 5,
            borderRadius: 20
        }}>
            <Image source={notification} style={{
                width: 15, height: 15
            }} />
        </View>
            <Text style={{ marginLeft: 5, fontSize: 10 }}>26/09/2019 às 11h10</Text></CardTitle>
        <View style={{ padding: 15 }}>
            <Text style={{ fontSize: 13 }}>Convide 2 amigos para o PEI e ganhe pontos para seu avatar</Text>
        </View>
        <View style={{ display: 'flex', flexDirection: 'row', justifyContent: 'space-around', alignItems: 'center' }}>
            <TouchableOpacity style={{ width: '72%' }}>
                <CardButton>Convidar amigos</CardButton>
            </TouchableOpacity>
        </View>
    </Card>
);

export default CardConvideAmigos;