import React from 'react';

import { View, TouchableOpacity, Text, Image } from 'react-native';
import { Card } from 'react-native-shadow-cards';
import { CardTitle, CardButton } from './styles';

const AulasCard = (props) => (
    <Card style={{
<<<<<<< HEAD
        display: 'flex', flexDirection: 'column', justifyContent: 'center',
        width: '47%', paddingBottom: 10, marginTop: 15,
        borderTopWidth: 2.5,
        borderTopColor: '#f46522'
    }} cornerRadius={0} elevation={3}>
        {props.hasUpdate && <View style={{ backgroundColor: '#f46522', width: '50%' }}><Text style={{ color: 'white', padding: 3 }}>Novas Aulas</Text></View>}
        <CardTitle style={{ paddingTop: 10 }}><Image source={props.image} style={{ height: props.height, width: props.width }} /><Text style={{ fontSize: 13, padding: 12 }}>{props.classes}</Text></CardTitle>
        <View style={{ display: 'flex', flexDirection: 'row', justifyContent: 'space-around', alignItems: 'center' }}>
            <TouchableOpacity style={{ width: '70%' }} onPress={() => props.props.navigation.push('ListaAulas')}>
                <CardButton>{props.subject}</CardButton>
=======
        width: '47%', paddingTop: 22, paddingBottom: 10, marginTop: 15,
        borderTopWidth: 2.5,
        borderTopColor: '#f46522'
    }} cornerRadius={0} elevation={3}>
        {props.novasAulas && <View style={{backgroundColor:'#f46522', width:'45%', position:'absolute', top:0, left:0, padding:2}}><Text style={{color:'white', fontSize:12, textAlign:'center'}}>Novas aulas</Text></View>}
        <CardTitle><Image source={props.image} style={{ height: props.height, width: props.width }} /><Text style={{ fontSize: 13, padding: 12 }}>{props.classes}</Text></CardTitle>
        <View style={{ display: 'flex', flexDirection: 'row', justifyContent: 'space-around', alignItems: 'center' }}>
            <TouchableOpacity style={{ width: '65%' }}>
                <CardButton style={props.subject=='Ling. Portuguesa' && {width:'110%'}}>{props.subject}</CardButton>
>>>>>>> 94521daee6d323f9cfc2b20aa9dc7ec040769234
            </TouchableOpacity>
        </View>
    </Card>
);

export default AulasCard;