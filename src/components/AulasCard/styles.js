import styled from 'styled-components'

export const CardTitle = styled.View`
    display: flex;
    justifyContent: center;
    alignItems: center;
    `;
export const CardButton = styled.Text`
  color: white;
  font-weight: bold;
  paddingTop: 5px;
  paddingBottom: 5px;
  textAlign: center;
  backgroundColor: #19ac2d;
  borderRadius: 30px;
`;