import React from 'react';

import { View, Text, Image } from 'react-native';
import { Card } from 'react-native-shadow-cards';
import Icon from "react-native-vector-icons/MaterialIcons";
import notification from '../../../assets/notifications-white.png'
import { CardTitle, CardButton } from './styles';

const EmProgressoCard = () => (
    <Card style={{
        width: '48%', paddingTop: 10, paddingBottom: 10, marginTop: 15,
        borderTopWidth: 2.5,
        backgroundColor: '#fcfcfc',
        borderTopColor: '#f46522'
    }} cornerRadius={0} elevation={3}>
        <View>
            <CardTitle style={{ backgroundColor: '#fff', width: '96%', justifyContent: 'flex-end', borderWidth: 1, borderColor: '#e0e0e0' }}>
                <Text style={{ color: '#19ac2d' }}>200xp</Text></CardTitle>
            <CardTitle style={{ position: 'absolute', backgroundColor: '#f46522' }}>
                <Text style={{ color: '#fff' }}>O leitor</Text>
                <Image source={notification} style={{ width: 20, height: 20 }} />
            </CardTitle>
        </View>
        <View style={{ padding: 15 }}>
            <Text style={{ fontSize: 13 }}>Faça 2 exercícios de interpretação de texto</Text>
        </View>
        <View style={{ display: 'flex', flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center', marginLeft: 15 }}>
            <CardButton style={{ borderRadius: 3, fontSize: 10, paddingTop: 0, paddingBottom: 0, width: '50%' }}>50%</CardButton>
        </View>
    </Card>
);

export default EmProgressoCard;
