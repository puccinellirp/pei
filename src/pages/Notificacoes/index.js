import React from 'react';

import { View, TouchableOpacity, Image, StatusBar } from 'react-native';
import NotificationsItem from '../../Validators/NotificationsItem'

import { Chat } from './styles';
import robot from '../../assets/Componente.png'

const Notificacoes = (props) => (
    <>
        <StatusBar barStyle='dark-content' backgroundColor='#fff' translucent />
        <View style={{ display: 'flex', flexDirection: 'row', justifyContent: 'space-between', flexWrap: 'wrap' }}>
            <NotificationsItem type='Novo-Simulado' teacher='Prof. Eugênio Hordones' text='Adicionou um novo simulado de Geografia' />
            <NotificationsItem type='Convide-Amigos' />
            <NotificationsItem type='Novo-Simulado' teacher='Prof. Eugênio Hordones' text='Adicionou um novo simulado de Matemática' />

            <NotificationsItem type='Novas-Aulas' />
        </View>
        <Chat>
            <TouchableOpacity style={{ position: 'absolute', bottom: 30, right: 20 }} onPress={() => props.navigation.push('Bot')}>
                <Image source={robot} style={{ width: 130, height: 50 }} />
            </TouchableOpacity>
        </Chat>
    </>
);

export default Notificacoes;
