import React from 'react';

import { View, Text, StatusBar, Image,TouchableOpacity } from 'react-native';
import chevronRight from '../../assets/chevron_right.png'
import chevronLeft from '../../assets/chevron_left.png'
import chevronDown from '../../assets/chevron_down_green.png'
import baseTrilha from '../../assets/baseTrilhaGeral.png'
import userMarker from '../../assets/userMarker.png'
import bonusCoin from '../../assets/bonusCoin.png'
import robot from '../../assets/Componente.png'
import { Chat } from './styles';

const Trilha = (props) => (
    <>
        <StatusBar barStyle='dark-content' backgroundColor='#fff' translucent />
        <View style={{display:'flex',alignItems:'center',backgroundColor:'#0c1c3f', borderBottomWidth:3, borderBottomColor:'#f46522', paddingBottom:10}}>
                <View style={{ display: 'flex', flexDirection: 'row', justifyContent: 'space-evenly', alignItems: 'center', width: '100%', padding: 10 }}>
                    <TouchableOpacity>
                        <Image source={chevronLeft} style={{ height: 12, width: 12 }} />
                    </TouchableOpacity>
                    <Text style={{ color: '#fff' }}>Trilha Outubro | Geral</Text>
                    <TouchableOpacity>
                        <Image source={chevronRight} style={{ height: 12, width: 12 }} />
                    </TouchableOpacity>
                </View>
                <TouchableOpacity>
                    <Image source={chevronDown} style={{height: 12, width:12}}/>
                </TouchableOpacity>
        </View>
        <View style={{width:'100%', height:'100%',position:'absolute', top:0,bottom:0,left:0,right:0,}}>
        <View style={{position: 'absolute', top:40,right:5,backgroundColor:'#19AC2D', padding:5, height:60,width:60, borderRadius:30}}>
            <Text style={{textAlign:'center', color:'white', fontWeight:'bold', fontSize:15}}>25</Text>
            <Text style={{textAlign:'center', color:'white', fontSize:10}}>Online Agora</Text>
        </View>
            <View style={{position: 'absolute', top:70,bottom:0, left:0,right:0}}>
                <View style={{padding:10}}>
                    <Text>Capitulo 1</Text>
                    <View style={{display:'flex', flexDirection:'row'}}>
                        <View style={{ width:15, height:15, backgroundColor:'#C530BB', borderRadius:7.5}}/>
                        <View style={{ width:15, height:15, backgroundColor:'#C530BB', borderRadius:7.5}} />
                        <View style={{ width:15, height:15, backgroundColor:'#C530BB', borderRadius:7.5}}/>
                        <View style={{ width:15, height:15,borderWidth:2, borderColor:'#C530BB', borderRadius:7.5}}/>
                        <View style={{ width:15, height:15,borderWidth:2, borderColor:'#C530BB', borderRadius:7.5}}/>
                    </View>
                    <Text>Capitulo 2</Text>
                    <View style={{display:'flex', flexDirection:'row'}}>
                        <View style={{ width:15, height:15, borderWidth:2, borderColor:'#24C56F', borderRadius:7.5}}/>
                        <View style={{ width:15, height:15, borderWidth:2, borderColor:'#24C56F', borderRadius:7.5}} />
                        <View style={{ width:15, height:15, borderWidth:2, borderColor:'#24C56F', borderRadius:7.5}}/>
                        <View style={{ width:15, height:15,borderWidth:2, borderColor:'#24C56F', borderRadius:7.5}}/>
                        <View style={{ width:15, height:15,borderWidth:2, borderColor:'#24C56F', borderRadius:7.5}}/>
                    </View>
                    <Text>Capitulo 3</Text>
                    <View style={{display:'flex', flexDirection:'row'}}>
                        <View style={{ width:15, height:15, borderWidth:2, borderColor:'#0A8B6C', borderRadius:7.5}}/>
                        <View style={{ width:15, height:15, borderWidth:2, borderColor:'#0A8B6C', borderRadius:7.5}} />
                        <View style={{ width:15, height:15, borderWidth:2, borderColor:'#0A8B6C', borderRadius:7.5}}/>
                        <View style={{ width:15, height:15,borderWidth:2, borderColor:'#0A8B6C', borderRadius:7.5}}/>
                        <View style={{ width:15, height:15,borderWidth:2, borderColor:'#0A8B6C', borderRadius:7.5}}/>
                    </View>
                    <Text>Capitulo 4</Text>
                    <View style={{display:'flex', flexDirection:'row'}}>
                        <View style={{ width:15, height:15,borderWidth:2, borderColor:'#F7A01E', borderRadius:7.5}}/>
                        <View style={{ width:15, height:15,borderWidth:2, borderColor:'#F7A01E', borderRadius:7.5}} />
                        <View style={{ width:15, height:15,borderWidth:2, borderColor:'#F7A01E', borderRadius:7.5}}/>
                        <View style={{ width:15, height:15,borderWidth:2, borderColor:'#F7A01E', borderRadius:7.5}}/>
                        <View style={{ width:15, height:15,borderWidth:2, borderColor:'#F7A01E', borderRadius:7.5}}/>
                    </View>
                </View> 
            </View>
            <Image source={baseTrilha} resizeMode="contain" style={{position:'absolute', top:0,bottom:0,left:0,right:0,height:'100%',width:'100%'}}/>
            <Image source={userMarker} resizeMode="contain" style={{position:'absolute', height:65,width:40, top:360, bottom:0, left:135,right:0 }}/>
            <Image source={bonusCoin} resizeMode="contain" style={{position:'absolute', height:65,width:40, top:255, bottom:0, left:170,right:0 }}/>
            <Image source={bonusCoin} resizeMode="contain" style={{position:'absolute', height:65,width:40, top:233, bottom:0, left:55,right:0 }}/>
            <Image source={bonusCoin} resizeMode="contain" style={{position:'absolute', height:65,width:40, top:320, bottom:0, left:210,right:0 }}/>
            <Image source={bonusCoin} resizeMode="contain" style={{position:'absolute', height:65,width:40, top:120, bottom:0, left:250,right:0 }}/>
        </View>
        <Chat>
            <TouchableOpacity style={{ position: 'absolute', bottom: 15, right: 15 }} onPress={() => props.navigation.push('Bot')}>
                <Image source={robot} style={{ width: 130, height: 50 }} />
            </TouchableOpacity>
        </Chat>
    </>
);

export default Trilha;
