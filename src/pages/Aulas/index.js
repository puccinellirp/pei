import React from 'react';

import { ScrollView, TouchableOpacity, Image, StatusBar } from 'react-native';
import AulasCard from '../../components/AulasCard'

import robot from '../../assets/Componente.png'

import matematica from '../../assets/matematica.png'
import portugues from '../../assets/portugues.png'
import biologia from '../../assets/biologia.png'
import quimica from '../../assets/quimica.png'
import fisica from '../../assets/fisica.png'
import artes from '../../assets/artes.png'
import historia from '../../assets/historia.png'
import filosofia from '../../assets/filosofia.png'
import sociologia from '../../assets/sociologia.png'
import ingles from '../../assets/ingles.png'

import { Chat } from './styles';

const Aulas = (props) => (
    <>
        <StatusBar barStyle='dark-content' backgroundColor='#fff' translucent />
        <ScrollView contentContainerStyle={{ display: 'flex', flexDirection: 'row', justifyContent: 'space-between', flexWrap: 'wrap', paddingBottom: 10 }}>
<<<<<<< HEAD
            <AulasCard classes='10 aulas disponíveis' subject='Matemática' hasUpdate={false} image={matematica} height={40} width={40} props={props} />
            <AulasCard classes='10 aulas disponíveis' subject='Ling. Portuguesa' hasUpdate={true} image={portugues} height={40} width={50} />
            <AulasCard classes='10 aulas disponíveis' subject='Biologia' hasUpdate={false} image={biologia} height={42} width={38} />
            <AulasCard classes='10 aulas disponíveis' subject='Química' hasUpdate={false} image={quimica} height={37} width={35} />
            <AulasCard classes='10 aulas disponíveis' subject='Física' hasUpdate={false} image={fisica} height={42} width={38} />
            <AulasCard classes='10 aulas disponíveis' subject='Artes' hasUpdate={false} image={artes} height={37} width={38} />
            <AulasCard classes='10 aulas disponíveis' subject='História' hasUpdate={false} image={historia} height={40} width={40} />
            <AulasCard classes='10 aulas disponíveis' subject='Filosofia' hasUpdate={false} image={filosofia} height={46} width={40} />
            <AulasCard classes='10 aulas disponíveis' subject='Sociologia' hasUpdate={false} image={sociologia} height={52} width={50} />
            <AulasCard classes='10 aulas disponíveis' subject='Inglês' hasUpdate={false} image={ingles} height={40} width={50} />
=======
            <AulasCard classes='10 aulas disponíveis' subject='Matemática' novasAulas={false} image={matematica} height={40} width={40} />
            <AulasCard classes='10 aulas disponíveis' subject='Ling. Portuguesa' novasAulas={true} image={portugues} height={40} width={50} />
            <AulasCard classes='10 aulas disponíveis' subject='Biologia' novasAulas={false} image={biologia} height={42} width={38} />
            <AulasCard classes='10 aulas disponíveis' subject='Química' novasAulas={false} image={quimica} height={37} width={35} />
            <AulasCard classes='10 aulas disponíveis' subject='Física' novasAulas={false} image={fisica} height={42} width={38} />
            <AulasCard classes='10 aulas disponíveis' subject='Artes' novasAulas={false} image={artes} height={37} width={38} />
            <AulasCard classes='10 aulas disponíveis' subject='História' novasAulas={false} image={historia} height={40} width={40} />
            <AulasCard classes='10 aulas disponíveis' subject='Filosofia' novasAulas={false} image={filosofia} height={46} width={40} />
            <AulasCard classes='10 aulas disponíveis' subject='Sociologia' novasAulas={false} image={sociologia} height={52} width={50} />
            <AulasCard classes='10 aulas disponíveis' subject='Inglês' novasAulas={false} image={ingles} height={40} width={50} />
>>>>>>> 94521daee6d323f9cfc2b20aa9dc7ec040769234
        </ScrollView>
        <Chat>
            <TouchableOpacity style={{ position: 'absolute', bottom: 30, right: 20 }} onPress={() => props.navigation.push('Bot')}>
                <Image source={robot} style={{ width: 130, height: 50 }} />
            </TouchableOpacity>
        </Chat>
    </>
);

export default Aulas;
