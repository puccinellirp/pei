import React from 'react';

import { View, TouchableOpacity, Text, ScrollView, Image } from 'react-native';
import Icon from "react-native-vector-icons/MaterialIcons";
import { Card } from 'react-native-shadow-cards';
import notification from '../../assets/notifications-white.png';
import checkbox from '../../assets/checkbox.png'
import { Container, CardTitle, CardButton, Chat } from './styles';

import robot from '../../assets/Componente.png'

const Simulados = (props) => (
    <>
        <ScrollView>
            <View style={{ paddingTop: 10, paddingBottom: 10, display: 'flex', alignItems: 'center', justifyContent: 'center' }}>
                <Text style={{ fontWeight: 'bold' }}>Simulados Disponíveis</Text>
                <Container>
                    <Card style={{ width: '100%', padding: 10, display: 'flex', flexDirection: 'row', justifyContent: 'space-between' }} cornerRadius={0} elevation={3}>
                        <CardTitle><View style={{
                            textAlign: 'center', backgroundColor: '#f46522',
                            padding: 5,
                            borderRadius: 20
                        }}>
                            <Image source={notification} style={{
                                width: 15, height: 15, textAlign: 'center'
                            }} />
                        </View>
                            <View style={{ display: 'flex', alignItems: 'flex-start', justifyContent: 'flex-start', marginLeft: 5 }}>
                                <Text>Geografica no Brasil</Text>
                                <Text style={{ fontSize: 12 }}>Simulado 21</Text>
                            </View>
                        </CardTitle>
                        <View style={{ display: 'flex', alignItems: 'center', width: '40%' }}>
                            <View style={{ display: 'flex', flexDirection: 'row', alignItems: 'flex-end', justifyContent: 'flex-end', width: '100%' }}>
                                <Text style={{ fontSize: 12 }}>Prêmio</Text>
                                <Text style={{ fontSize: 12, fontWeight: 'bold', marginLeft: 3 }}>200xp</Text>
                            </View>
                            <TouchableOpacity style={{ width: '100%' }}>
                                <CardButton>Fazer simulado</CardButton>
                            </TouchableOpacity>
                            <View style={{ display: 'flex', flexDirection: 'row', alignItems: 'flex-end', justifyContent: 'flex-end', width: '100%' }}>
                                <Text style={{ fontSize: 12 }}>20 perguntas</Text>
                            </View>
                        </View>
                    </Card>
                </Container>

                <Container>
                    <Card style={{ width: '100%', padding: 10, display: 'flex', flexDirection: 'row', justifyContent: 'space-between' }} cornerRadius={0} elevation={3}>
                        <CardTitle><View style={{
                            textAlign: 'center', backgroundColor: '#f46522',
                            padding: 5,
                            borderRadius: 20
                        }}>
                            <Image source={notification} style={{
                                width: 15, height: 15, textAlign: 'center'
                            }} />
                        </View>
                            <View style={{ display: 'flex', alignItems: 'flex-start', justifyContent: 'flex-start', marginLeft: 5 }}>
                                <Text>Biologia Marinha</Text>
                                <Text style={{ fontSize: 12 }}>Simulado 20</Text>
                            </View>
                        </CardTitle>
                        <View style={{ display: 'flex', alignItems: 'center', width: '40%' }}>
                            <View style={{ display: 'flex', flexDirection: 'row', alignItems: 'flex-end', justifyContent: 'flex-end', width: '100%' }}>
                                <Text style={{ fontSize: 12 }}>Prêmio</Text>
                                <Text style={{ fontSize: 12, fontWeight: 'bold', marginLeft: 3 }}>200xp</Text>
                            </View>
                            <TouchableOpacity style={{ width: '100%' }}>
                                <CardButton>Fazer simulado</CardButton>
                            </TouchableOpacity>
                            <View style={{ display: 'flex', flexDirection: 'row', alignItems: 'flex-end', justifyContent: 'flex-end', width: '100%' }}>
                                <Text style={{ fontSize: 12 }}>20 perguntas</Text>
                            </View>
                        </View>
                    </Card>
                </Container>
            </View>
            <View style={{ paddingTop: 10, paddingBottom: 10, display: 'flex', alignItems: 'center', justifyContent: 'center' }}>
                <Text style={{ fontWeight: 'bold' }}>Simulados Concluídos</Text>
                <Container style={{ borderTopColor: "#19ac2d" }}>
                    <Card style={{ width: '100%', padding: 10, display: 'flex', flexDirection: 'row', justifyContent: 'space-between' }} cornerRadius={0} elevation={3}>
                        <CardTitle><Image source={checkbox} style={{ width: 20, height: 20 }} />
                            <View style={{ display: 'flex', alignItems: 'flex-start', justifyContent: 'flex-start', marginLeft: 5 }}>
                                <Text>Hifen e Virgula</Text>
                                <Text style={{ fontSize: 12 }}>Simulado 19</Text>
                            </View>
                        </CardTitle>
                        <View style={{ display: 'flex', width: '45%' }}>
                            <View style={{ display: 'flex', flexDirection: 'row', alignItems: 'center', justifyContent: 'center', width: '100%' }}>
                                <Text style={{ color: "#19ac2d" }}>Concluído</Text>
                            </View>
                            <View style={{ display: 'flex', flexDirection: 'row', alignItems: 'center', justifyContent: 'center', width: '100%' }}>
                                <Text style={{ fontSize: 12 }}>20 perguntas | 18 acertos</Text>
                            </View>
                        </View>
                    </Card>
                </Container>
                <Container style={{ borderTopColor: "#19ac2d" }}>
                    <Card style={{ width: '100%', padding: 10, display: 'flex', flexDirection: 'row', justifyContent: 'space-between' }} cornerRadius={0} elevation={3}>
                        <CardTitle><Image source={checkbox} style={{ width: 20, height: 20 }} />
                            <View style={{ display: 'flex', alignItems: 'flex-start', justifyContent: 'flex-start', marginLeft: 5 }}>
                                <Text>Hifen e Virgula</Text>
                                <Text style={{ fontSize: 12 }}>Simulado 19</Text>
                            </View>
                        </CardTitle>
                        <View style={{ display: 'flex', width: '45%' }}>
                            <View style={{ display: 'flex', flexDirection: 'row', alignItems: 'center', justifyContent: 'center', width: '100%' }}>
                                <Text style={{ color: "#19ac2d" }}>Concluído</Text>
                            </View>
                            <View style={{ display: 'flex', flexDirection: 'row', alignItems: 'center', justifyContent: 'center', width: '100%' }}>
                                <Text style={{ fontSize: 12 }}>20 perguntas | 18 acertos</Text>
                            </View>
                        </View>
                    </Card>
                </Container>
                <Container style={{ borderTopColor: "#19ac2d" }}>
                    <Card style={{ width: '100%', padding: 10, display: 'flex', flexDirection: 'row', justifyContent: 'space-between' }} cornerRadius={0} elevation={3}>
                        <CardTitle><Image source={checkbox} style={{ width: 20, height: 20 }} />
                            <View style={{ display: 'flex', alignItems: 'flex-start', justifyContent: 'flex-start', marginLeft: 5 }}>
                                <Text>Hifen e Virgula</Text>
                                <Text style={{ fontSize: 12 }}>Simulado 19</Text>
                            </View>
                        </CardTitle>
                        <View style={{ display: 'flex', width: '45%' }}>
                            <View style={{ display: 'flex', flexDirection: 'row', alignItems: 'center', justifyContent: 'center', width: '100%' }}>
                                <Text style={{ color: "#19ac2d" }}>Concluído</Text>
                            </View>
                            <View style={{ display: 'flex', flexDirection: 'row', alignItems: 'center', justifyContent: 'center', width: '100%' }}>
                                <Text style={{ fontSize: 12 }}>20 perguntas | 18 acertos</Text>
                            </View>
                        </View>
                    </Card>
                </Container>
                <Container style={{ borderTopColor: "#19ac2d" }}>
                    <Card style={{ width: '100%', padding: 10, display: 'flex', flexDirection: 'row', justifyContent: 'space-between' }} cornerRadius={0} elevation={3}>
                        <CardTitle><Image source={checkbox} style={{ width: 20, height: 20 }} />
                            <View style={{ display: 'flex', alignItems: 'flex-start', justifyContent: 'flex-start', marginLeft: 5 }}>
                                <Text>Hifen e Virgula</Text>
                                <Text style={{ fontSize: 12 }}>Simulado 19</Text>
                            </View>
                        </CardTitle>
                        <View style={{ display: 'flex', width: '45%' }}>
                            <View style={{ display: 'flex', flexDirection: 'row', alignItems: 'center', justifyContent: 'center', width: '100%' }}>
                                <Text style={{ color: "#19ac2d" }}>Concluído</Text>
                            </View>
                            <View style={{ display: 'flex', flexDirection: 'row', alignItems: 'center', justifyContent: 'center', width: '100%' }}>
                                <Text style={{ fontSize: 12 }}>20 perguntas | 18 acertos</Text>
                            </View>
                        </View>
                    </Card>
                </Container>
                <Container style={{ borderTopColor: "#19ac2d" }}>
                    <Card style={{ width: '100%', padding: 10, display: 'flex', flexDirection: 'row', justifyContent: 'space-between' }} cornerRadius={0} elevation={3}>
                        <CardTitle><Image source={checkbox} style={{ width: 20, height: 20 }} />
                            <View style={{ display: 'flex', alignItems: 'flex-start', justifyContent: 'flex-start', marginLeft: 5 }}>
                                <Text>Hifen e Virgula</Text>
                                <Text style={{ fontSize: 12 }}>Simulado 19</Text>
                            </View>
                        </CardTitle>
                        <View style={{ display: 'flex', width: '45%' }}>
                            <View style={{ display: 'flex', flexDirection: 'row', alignItems: 'center', justifyContent: 'center', width: '100%' }}>
                                <Text style={{ color: "#19ac2d" }}>Concluído</Text>
                            </View>
                            <View style={{ display: 'flex', flexDirection: 'row', alignItems: 'center', justifyContent: 'center', width: '100%' }}>
                                <Text style={{ fontSize: 12 }}>20 perguntas | 18 acertos</Text>
                            </View>
                        </View>
                    </Card>
                </Container>
                <Container style={{ borderTopColor: "#19ac2d" }}>
                    <Card style={{ width: '100%', padding: 10, display: 'flex', flexDirection: 'row', justifyContent: 'space-between' }} cornerRadius={0} elevation={3}>
                        <CardTitle><Image source={checkbox} style={{ width: 20, height: 20 }} />
                            <View style={{ display: 'flex', alignItems: 'flex-start', justifyContent: 'flex-start', marginLeft: 5 }}>
                                <Text>Hifen e Virgula</Text>
                                <Text style={{ fontSize: 12 }}>Simulado 19</Text>
                            </View>
                        </CardTitle>
                        <View style={{ display: 'flex', width: '45%' }}>
                            <View style={{ display: 'flex', flexDirection: 'row', alignItems: 'center', justifyContent: 'center', width: '100%' }}>
                                <Text style={{ color: "#19ac2d" }}>Concluído</Text>
                            </View>
                            <View style={{ display: 'flex', flexDirection: 'row', alignItems: 'center', justifyContent: 'center', width: '100%' }}>
                                <Text style={{ fontSize: 12 }}>20 perguntas | 18 acertos</Text>
                            </View>
                        </View>
                    </Card>
                </Container>
                <Container style={{ borderTopColor: "#19ac2d" }}>
                    <Card style={{ width: '100%', padding: 10, display: 'flex', flexDirection: 'row', justifyContent: 'space-between' }} cornerRadius={0} elevation={3}>
                        <CardTitle><Image source={checkbox} style={{ width: 20, height: 20 }} />
                            <View style={{ display: 'flex', alignItems: 'flex-start', justifyContent: 'flex-start', marginLeft: 5 }}>
                                <Text>Hifen e Virgula</Text>
                                <Text style={{ fontSize: 12 }}>Simulado 19</Text>
                            </View>
                        </CardTitle>
                        <View style={{ display: 'flex', width: '45%' }}>
                            <View style={{ display: 'flex', flexDirection: 'row', alignItems: 'center', justifyContent: 'center', width: '100%' }}>
                                <Text style={{ color: "#19ac2d" }}>Concluído</Text>
                            </View>
                            <View style={{ display: 'flex', flexDirection: 'row', alignItems: 'center', justifyContent: 'center', width: '100%' }}>
                                <Text style={{ fontSize: 12 }}>20 perguntas | 18 acertos</Text>
                            </View>
                        </View>
                    </Card>
                </Container>
                <Container style={{ borderTopColor: "#19ac2d" }}>
                    <Card style={{ width: '100%', padding: 10, display: 'flex', flexDirection: 'row', justifyContent: 'space-between' }} cornerRadius={0} elevation={3}>
                        <CardTitle><Image source={checkbox} style={{ width: 20, height: 20 }} />
                            <View style={{ display: 'flex', alignItems: 'flex-start', justifyContent: 'flex-start', marginLeft: 5 }}>
                                <Text>Hifen e Virgula</Text>
                                <Text style={{ fontSize: 12 }}>Simulado 19</Text>
                            </View>
                        </CardTitle>
                        <View style={{ display: 'flex', width: '45%' }}>
                            <View style={{ display: 'flex', flexDirection: 'row', alignItems: 'center', justifyContent: 'center', width: '100%' }}>
                                <Text style={{ color: "#19ac2d" }}>Concluído</Text>
                            </View>
                            <View style={{ display: 'flex', flexDirection: 'row', alignItems: 'center', justifyContent: 'center', width: '100%' }}>
                                <Text style={{ fontSize: 12 }}>20 perguntas | 18 acertos</Text>
                            </View>
                        </View>
                    </Card>
                </Container>
            </View>
        </ScrollView>
        <Chat>
            <TouchableOpacity style={{ position: 'absolute', bottom: 30, right: 20 }} onPress={() => props.navigation.push('Bot')}>
                <Image source={robot} style={{ width: 130, height: 50 }} />
            </TouchableOpacity>
        </Chat>
    </>
);

export default Simulados;
