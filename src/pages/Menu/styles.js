import styled from 'styled-components/native';

export const ItemTitle = styled.Text`

`

export const Container = styled.View`
    flexDirection: column;
    backgroundColor: #fff;
    marginTop:auto;
    marginBottom: auto;
`

export const ItemRow = styled.View`
    display:flex;
    flexDirection: row;
    justifyContent: space-around;
    marginBottom: 10%;
`

export const Item = styled.TouchableOpacity`
`
export const Robot = styled.TouchableOpacity`
    position: absolute;
    bottom:30;
    right:20;
`