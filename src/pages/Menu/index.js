import React, { useState } from 'react';
import Icon from "react-native-vector-icons/MaterialIcons";
import { View, StatusBar, Image } from 'react-native';
import { Badge } from 'react-native-elements';

import flag from '../../assets/flag.png';
import feed from '../../assets/timeline.png';
import notificacoes from '../../assets/notifications.png'
import trilha from '../../assets/play_inline.png'
import school from '../../assets/school.png';
import simulate from '../../assets/simulate.png';
import avatar from '../../assets/grupo.png';
import perfil from '../../assets/pedro.png';
import robot from '../../assets/Componente.png';


import { Container, ItemTitle, ItemRow, Item, Robot } from './styles';
function Menu(props) {
    const [notify, SetNotify] = useState(true);
    return (
        <>
            <StatusBar barStyle='light-content' backgroundColor='#f46522' translucent />
            <Container>
                <ItemRow>
                    <Item style={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }} onPress={() => props.navigation.push('Feed')}>
                        <ItemTitle>Feed</ItemTitle>
                        <View>
                            <Image source={feed} style={{ width: 47, height: 26 }} />
                            {
                                notify ?
                                    <Badge
                                        status="error"
                                        containerStyle={{ position: 'absolute', top: 20, right: -12 }}
                                        value="Novo"
                                    /> : <View />
                            }
                        </View>
                    </Item>
                    <Item style={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }} onPress={() => props.navigation.navigate('Notificacoes')}>
                        <ItemTitle>Notificações</ItemTitle>
                        <View>
                            <Image source={notificacoes} style={{ width: 30, height: 35 }} />
                            {
                                notify ?
                                    <Badge
                                        status="error"
                                        containerStyle={{ position: 'absolute', top: 23, right: -10 }}
                                        value="1"
                                    /> : <View />
                            }
                        </View>
                    </Item>
                </ItemRow>
                <ItemRow>
                    <Item style={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }} onPress={() => props.navigation.push('Trilha')} style={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
                        <ItemTitle>Trilha</ItemTitle>
                        <Image source={trilha} style={{ width: 38, height: 38 }} />
                        {
                            notify ?
                                <Badge
                                    status="error"
                                    containerStyle={{ position: 'absolute', top: 45, right: -3 }}
                                    value="1"
                                /> : <View />
                        }
                    </Item>
                    <Item style={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }} onPress={() => props.navigation.push('Conquistas')}>
                        <ItemTitle>Conquistas</ItemTitle>
                        <Image source={flag} style={{ width: 38, height: 38 }} />
                        {
                            notify ?
                                <Badge
                                    status="error"
                                    containerStyle={{ position: 'absolute', top: 46, right: 5 }}
                                    value="99+"
                                /> : <View />
                        }
                    </Item>
                </ItemRow>
                <ItemRow style={{ width: '95%' }}>
                    <Item style={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }} onPress={() => props.navigation.push('Simulados')} >
                        <ItemTitle>Simulados</ItemTitle>
                        <Image source={simulate} style={{ width: 38, height: 38 }} />
                        {
                            notify ?
                                <Badge
                                    status="error"
                                    containerStyle={{ position: 'absolute', top: 48, right: 5 }}
                                    value="99+"
                                /> : <View />
                        }
                    </Item>
                    <Item style={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }} onPress={() => props.navigation.push('Aulas')}>
                        <ItemTitle>Aulas</ItemTitle>
                        <Image source={school} style={{ width: 38, height: 38 }} />
                        {
                            notify ?
                                <Badge
                                    status="error"
                                    containerStyle={{ position: 'absolute', top: 48, right: -10 }}
                                    value="99+"
                                /> : <View />
                        }
                    </Item>
                </ItemRow>
                <ItemRow>
                    <Item style={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }} onPress={() => props.navigation.push('Avatar')}>
                        <ItemTitle>Avatar</ItemTitle>
                        <Image source={avatar} style={{ width: 40, height: 40 }} />
                        {
                            notify ?
                                <Badge
                                    status="error"
                                    containerStyle={{ position: 'absolute', top: 55, right: -12 }}
                                    value="99+"
                                /> : <View />
                        }
                    </Item>
                    <Item style={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }} onPress={() => props.navigation.push('Perfil')} >
                        <ItemTitle>Perfil</ItemTitle>
                        <Image source={perfil} style={{ width: 45, height: 45 }} />
                        {
                            notify ?
                                <Badge
                                    status="error"
                                    containerStyle={{ position: 'absolute', top: 55, right: -10 }}
                                    value="99+"
                                /> : <View />
                        }
                    </Item>
                </ItemRow>
            </Container>
            <Robot onPress={() => props.navigation.push('Bot')}>
                <Image source={robot} style={{ width: 130, height: 50 }} />
            </Robot>
        </>
    );

}


export default Menu;
