import styled from 'styled-components'

export const Chat = styled.View`
    position: absolute;
    bottom: 0; right:0;
    display:flex;
    padding: 10px;
    flexDirection: row;
    justifyContent: flex-end;
    alignItems: center;
`