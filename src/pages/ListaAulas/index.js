import React from 'react';
import { ScrollView, StatusBar, Image, TouchableOpacity } from 'react-native';
import ListaAulasCard from '../../components/ListaAulasCard'
import robot from '../../assets/Componente.png'
import { Chat } from './styles';

const ListaSimulados = (props) => (
    <>
        <StatusBar barStyle='dark-content' backgroundColor='#ffffff' translucent />
        <ScrollView>
            <ListaAulasCard read={false} name="Pronomes Reflexivos" />
            <ListaAulasCard read={false} name="Ponto de Interrogação?" />
            <ListaAulasCard read={true} name="Vícios de Linguagem" />
            <ListaAulasCard read={true} name="Quando usar a Próclise" />
            <ListaAulasCard read={true} name="Emprego do Hífen" />
            <ListaAulasCard read={true} name="A Argumentação" />
            <ListaAulasCard read={true} name="Dicas de Redação" />
        </ScrollView>
        <Chat>
            <TouchableOpacity style={{ position: 'absolute', bottom: 30, right: 20 }} onPress={() => props.navigation.push('Bot')}>
                <Image source={robot} style={{ width: 130, height: 50 }} />
            </TouchableOpacity>
        </Chat>
    </>
)

export default ListaSimulados;
