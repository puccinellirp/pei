import React from 'react';

import { View, Image, Text, StatusBar, TouchableOpacity, ScrollView } from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { Card } from 'react-native-shadow-cards';

import avatar from '../../assets/avatarted.png'
import camera from '../../assets/camera.png'
import robot from '../../assets/Componente.png'
import acessorios from '../../assets/tshirt.png'
import upgrade from '../../assets/seta.png'
import novo from '../../assets/avataregg.png'
import bg from '../../assets/bgavatar.png'
import ted from '../../assets/Ted.png'
import lisa from '../../assets/lisa.png'
import bob from '../../assets/bob.png'
import cleo from '../../assets/cleo.png'
import paul from '../../assets/paul.png'
import julia from '../../assets/julia.png'

import { Chat } from './styles';

const Avatar = (props) => (
    <>
        <StatusBar barStyle='dark-content' backgroundColor='#fff' translucent />
        <Image source={bg} style={{ height: '100%', width: '100%', position: 'absolute' }} />
        <ScrollView style={{ marginBottom: 10 }}>
            <View style={{ display: 'flex', alignItems: 'center', marginTop: '10%' }} cornerRadius={1} elevation={5}>
                <Card style={{ width: '30%', height: '5%', padding: 1, marginBottom: -15 }} backgroundColor='green' cornerRadius={100} elevation={10}>
                    <Text style={{ color: 'white', fontSize: 20, fontWeight: 'bold', textAlign: 'center' }}>Ted</Text>
                </Card>
                <Card style={{ width: '80%', padding: 10 }} cornerRadius={10} elevation={5}>
                    <TouchableOpacity style={{ position: 'absolute', top: -15, right: -10, backgroundColor: 'white', padding: 9, borderRadius: 50, borderColor: 'black', borderWidth: 0.1 }}>
                        <Image source={camera} style={{ height: 22, width: 25, }} />
                    </TouchableOpacity>
                    <View style={{ display: 'flex', flexDirection: 'row', justifyContent: 'space-between', paddingTop: 30, padding: 10 }}>
                        <Image source={avatar} resizeMode='contain' style={{ height: '103%', width: '50%', marginLeft: '10%' }} />
                        <View>
                            <TouchableOpacity style={{ display: 'flex', alignItems: 'center' }}>
                                <Image source={acessorios} style={{ height: 50, width: 54 }} />
                                <Text style={{ fontSize: 12 }}>Acessórios</Text>
                            </TouchableOpacity>
                            <TouchableOpacity style={{ display: 'flex', alignItems: 'center', marginTop: 15 }}>
                                <Text style={{ fontSize: 10 }}>757/1000</Text>
                                <Image source={upgrade} style={{ height: 45, width: 50 }} />
                                <Text style={{ fontWeight: 'bold', fontSize: 12 }}>Upgrade</Text>
                                <Text style={{ fontSize: 10 }}>Nível 3</Text>
                            </TouchableOpacity>
                            <TouchableOpacity style={{ display: 'flex', alignItems: 'center', marginTop: 15 }}>
                                <Image source={novo} style={{ height: 55, width: 34 }} />
                                <Text style={{ fontSize: 12 }}>Novo</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                    <View style={{ display: 'flex', alignItems: 'center' }}>
                        <Text style={{ backgroundColor: '#19ac2d', color: 'white', padding: 5, borderRadius: 50 }}>Pontos: 757</Text>
                    </View>
                    <View style={{ display: 'flex', backgroundColor: 'orange', height: 2, marginTop: 8 }} />
                    <View style={{ display: 'flex', flexDirection: 'column', justifyContent: 'center' }}>
                        <View style={{ display: 'flex', flexDirection: 'row', justifyContent: 'space-around', marginTop: 10 }}>
                            <View>
                                <TouchableOpacity style={{ backgroundColor: '#f0e10f', padding: 5, borderRadius: 50 }}>
                                    <Image source={ted} style={{ height: 50, width: 50 }} />
                                </TouchableOpacity>
                                <Text style={{ textAlign: 'center' }}>Ted</Text>
                            </View>
                            <View>
                                <TouchableOpacity style={{ backgroundColor: '#dfdfdf', padding: 5, borderRadius: 50 }}>
                                    <Image source={lisa} style={{ height: 50, width: 50 }} />
                                </TouchableOpacity>
                                <Text style={{ textAlign: 'center' }}>Lisa</Text>
                            </View>
                            <View>
                                <TouchableOpacity style={{ backgroundColor: '#dfdfdf', padding: 5, borderRadius: 50 }}>
                                    <Image source={bob} style={{ height: 50, width: 50 }} />
                                </TouchableOpacity>
                                <Text style={{ textAlign: 'center' }}>Bob</Text>
                            </View>
                        </View>
                        <View style={{ display: 'flex', flexDirection: 'row', justifyContent: 'space-around', marginTop: 12, flexWrap: 'wrap' }}>
                            <View>
                                <TouchableOpacity style={{ backgroundColor: '#dfdfdf', padding: 5, borderRadius: 50 }}>
                                    <Image source={cleo} style={{ height: 50, width: 50 }} />
                                </TouchableOpacity>
                                <Text style={{ textAlign: 'center' }}>Cléo</Text>
                            </View>
                            <View>
                                <TouchableOpacity style={{ backgroundColor: '#dfdfdf', padding: 5, borderRadius: 50 }}>
                                    <Image source={paul} style={{ height: 50, width: 50 }} />
                                </TouchableOpacity>
                                <Text style={{ textAlign: 'center' }}>Paul</Text>
                            </View>
                            <View>
                                <TouchableOpacity style={{ backgroundColor: '#dfdfdf', padding: 5, borderRadius: 50 }}>
                                    <Image source={julia} style={{ height: 50, width: 50 }} />
                                </TouchableOpacity>
                                <Text style={{ textAlign: 'center' }}>Júlia</Text>
                            </View>
                        </View>
                    </View>
                </Card>
            </View>
        </ScrollView>
        <Chat>
            <TouchableOpacity style={{ position: 'absolute', bottom: 15, right: 15 }} onPress={() => props.navigation.push('Bot')}>
                <Image source={robot} style={{ width: 130, height: 50 }} />
            </TouchableOpacity>
        </Chat>
    </>
);

export default Avatar;
