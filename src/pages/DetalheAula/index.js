import React from 'react';

import { View, TouchableOpacity, Text, Image } from 'react-native';
import { Card } from 'react-native-shadow-cards';
import notification from '../../assets/notifications-white.png'
import { Container, CardTitle, CardButton } from './styles';

const DetalheAula = (props) => (
    <Container ContentContainerStyle={{ borderTopColor: '#f46522', marginTop: 20 }}>
        <Card style={{ width: '100%', padding: 20, marginBottom: 10 }} cornerRadius={0} elevation={3}>
            <View style={{ display: 'flex', alignItems: 'center' }}>
                <CardTitle>
                    <View style={{
                        textAlign: 'center', backgroundColor: '#f46522',
                        padding: 5,
                        borderRadius: 30
                    }}>
                        <Image source={notification} style={{
                            width: 25, height: 25
                        }} />
                    </View>
                </CardTitle>
                <View
                    style={{
                        borderBottomColor: 'black',
                        borderBottomWidth: 1,
                        width: '80%'
                    }}
                />
                <Text style={{ fontWeight: 'bold', fontSize: 20, color: '#707070' }}>Pronomes Reflexivos</Text>
                <View style={{ paddingTop: 15, paddingBottom: 15 }}>
                    <Text style={{ margin: 5, fontSize: 15 }}>
                        Os pronomes reflexivos indicam que o sujeito pratica uma ação verbal sobre si mesmo.
                    </Text>
                    <Text>Exemplos</Text>
                    <Text>•Olhei-me ao espelho e vi que estava realmente pálida.</Text>
                    <Text>•Penteou-se e saiu.</Text>
                </View>

                <View
                    style={{
                        borderBottomColor: 'black',
                        borderBottomWidth: 1,
                        width: '80%'
                    }}
                />
                <View style={{ paddingTop: 15, paddingBottom: 15 }}>
                    <Text>Os Pronomes Reflexivos são:  se , si e consigo. Além desses, há pronomes oblíquos átonos que assumem essa função: me, te, nos e vos.</Text>
                    <Text>Exemplos</Text>
                    <Text>•Levantou-se, vestiu-se e correu para pegar o ônibus</Text>
                    <Text>•Eles banharam-se e voltaram para a toalha para apanhar sol.</Text>
                    <Text>•Odiei-me pela atitude que tive ontem.</Text>
                    <Text>•Acochegamo-nos no sofá e assistimos um filme</Text>
                    <Text>•Levantai-vos!</Text>
                    <Text>•Uma vez que o sujeito pratica e recebe a ação, o objeto representa a mesma pessoa ou a mesma coisa que o sujeito, de modo que o pronome reflexivo pode funcionar como objeto direto ou indireto.</Text>
                </View>
                <View
                    style={{
                        borderBottomColor: 'black',
                        borderBottomWidth: 1,
                        width: '80%'
                    }}
                />
                <TouchableOpacity style={{ width: '40%', display: 'flex', alignItems: 'center' }}>
                    <CardButton>Vamos Praticar!</CardButton>
                </TouchableOpacity>
            </View>
        </Card>
    </Container>
);

export default DetalheAula;
